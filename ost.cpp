/** Голова змеи
 * @author Dmitriy
 */
public class Head extends Segment{
    
    /** Создает экземпляр головы
    * @param x - координата левого угла изображения
    */
    public Head(int x,int y){
        super(x,y);
    }

    /** Обновление
     * @param dt - время между кадрами 
     * @param prevPos - позиция прошлого сегмента
     * @param prevNav - направление прошлого сегмента
     */
    @Override
    public void update(float dt, Vector2 prevPos, Navigate prevNav) {
        if(position.x%24==0 && position.y%24==0)
            nav=nextNav;
        else 
            setNextNav();
        setNavigate(dt);
        if(tail!=null)
            tail.update(dt,position,nav);
    }
    
    /** Двигает змейку в текущем направлении
     * @param dt - время между кадрами
     */
    protected void setNavigate(float dt){
        switch (nav){
            case UP:
                toUp(dt);
                break;
            case DOWN:
                toDown(dt);
            case RIGHT:
                toRight(dt);
                break;
            case LEFT:
                toLeft(dt);
                break;
        }
    }

    /** Устанавливает следующее направление
     */
    private void setNextNav() {
        if(Gdx.input.isKeyPressed(Keys.UP) && nav!=Navigate.DOWN)
            nextNav=Navigate.UP;
        else if(Gdx.input.isKeyPressed(Keys.DOWN) && nav!=Navigate.UP)
            nextNav=Navigate.DOWN;
        else if(Gdx.input.isKeyPressed(Keys.RIGHT) && nav!=Navigate.LEFT)
            nextNav=Navigate.RIGHT;
        else if(Gdx.input.isKeyPressed(Keys.LEFT) && nav!=Navigate.RIGHT)
            nextNav=Navigate.LEFT;
    }
}